//
//  ViewController.m
//  PresentViewControllerToRootDemo
//
//  Created by 邱学伟 on 2016/11/4.
//  Copyright © 2016年 邱学伟. All rights reserved.
//

#import "ViewController.h"
#import "A_VC.h"
#import "C_VC.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
}
- (IBAction)toA:(id)sender {
    A_VC *aVC = [[A_VC alloc] init];
    [self presentViewController:aVC animated:YES completion:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
