//
//  A_VC.m
//  PresentViewControllerToRootDemo
//
//  Created by 邱学伟 on 2016/11/4.
//  Copyright © 2016年 邱学伟. All rights reserved.
//

#import "A_VC.h"
#import "B_VC.h"
@interface A_VC ()

@end

@implementation A_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)toB:(id)sender {
    [self presentViewController:[[B_VC alloc]init] animated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
