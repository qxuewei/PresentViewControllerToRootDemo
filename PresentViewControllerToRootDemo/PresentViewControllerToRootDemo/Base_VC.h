//
//  Base_VC.h
//  PresentViewControllerToRootDemo
//
//  Created by 邱学伟 on 2016/11/4.
//  Copyright © 2016年 邱学伟. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Base_VC : UIViewController
//暴露返回跟控制器的对象方法
-(void)toRootViewController;
@end
