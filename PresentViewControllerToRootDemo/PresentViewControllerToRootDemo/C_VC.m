//
//  C_VC.m
//  PresentViewControllerToRootDemo
//
//  Created by 邱学伟 on 2016/11/4.
//  Copyright © 2016年 邱学伟. All rights reserved.
//

#import "C_VC.h"

@interface C_VC ()

@end

@implementation C_VC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)back:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)toRoot:(id)sender {
    [self toRootViewController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
